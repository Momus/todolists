* Todolists Empty Application Environment
  ** for the Coursera-Johns Hopkins "Rails with Active Record and Action Pack"

*** Usage 
        
This repository contains a pre-built Ruby on Rails
application with the correct versions of gems in order to
      successfully submit a completed assignment.
          
    **** Requirements
        1. Docker
        2. Docker Compose

    **** Installation
        If requirements are met, just clone this repository.

    **** Usage
        1. Three choices to  get the image: 
           a. build with the included Dockerfile, pull it
           b. from my dockerhub repo:
               '''sh
               docker pull momus/todolists
               ''''
           c. let the docker-compose step below build it for you.
       2. start up docker-compose from terminal session in the same
       directory (the ampersand puts the task in the background):
       '''sh
       docker-compose up &
       3. run your commands, but prefix them with the correct
       docker-compose command:
       '''sh
       docker-compose exec app rake db:migrate
       ''''
       3. to shut down or restart your server:
       '''sh
       docker-compose down
       '''
       4. The docker container creates new files that are owned by the
       system (root) user. These will be read-only until you change
       their permissions.  Do it for all files at once:
       '''sh
       sudo chown -R myname:mygroup .
       ''''
       5. It's probably a good idea to clone this repository separately
       for each assignment. You will need to copy over the specs from
       each assignment yourself as per instructions.
       
       6. DO NOT copy over the `Gemfile` DO NOT run `bundle`
       anything---especially not `install` or `update.` If you need
       more/different Gems (you don't for the class) you'll have to
       modify the Gemfile then re-build the image. The image will
       automatically prefix whatever the commands you pass it with
       `bundle exec` so that is not necessary.
       
       7. The Rails server automatically starts when you run
       `docker-compose up` Navigate to `http://localhost:3000` to see
       it. To restart, see Step 3.
       
       
               
               
