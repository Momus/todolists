FROM ruby:2.2.3
LABEL maintainer="dmitri@momus.net"

#apt-get update returns a non-zero return code (100) but still seems
#to work
RUN apt-get update ; apt get upgrade ; apt-get install -y \ 
  build-essential \ 
  nodejs


RUN mkdir -p /app 
WORKDIR /app

COPY Gemfile Gemfile.lock  ./
RUN gem install bundler -v '~> 1' && \
        gem install rspec-support -v '3.8.0' ;\
        bundle install --jobs 20 --retry 5

COPY . ./

EXPOSE 3000


ENTRYPOINT ["bundle", "exec"]
CMD ["rails", "server", "-b", "0.0.0.0"]
